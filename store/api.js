getBaseUrl = baseurl => {
  return baseurl || "http://172.31.144.200:8080/api/";
  // return baseurl || "https://groomup.herokuapp.com/api/";
};

export const getList = (token, url, query, baseurl) => {
  console.log(token);
  let builtQuery = null;
  let destUrl = "";
  let theBaseUrl = getBaseUrl(baseurl);

  if (query) {
    for (let key in query) {
      builtQuery += key + "&" + query[key];
    }
  }
  if (!builtQuery) {
    destUrl = theBaseUrl + url;
  }

  if (builtQuery) {
    destUrl = theBaseUrl + url + "?" + builtQuery;
  }

  let payload = {
    method: "GET"
  };

  if (token) {
    payload.headers = {};
    payload.headers.Authorization = `Bearer ${token}`;
  }

  return fetch(destUrl, payload);
};

export const getItem = (token, url, id, baseurl) => {
  let theBaseUrl = getBaseUrl(baseurl);

  let destUrl = theBaseUrl + url;
  if (id) {
    destUrl = destUrl + "/" + id;
  }

  return fetch(destUrl, {
    method: "GET",
    headers: { Authorization: `Bearer ${token}` }
  });
};

export const postItem = (token, data, url, baseurl) => {
  let theBaseUrl = getBaseUrl(baseurl);
  // console.log(theBaseUrl + url);
  let payload = {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    method: "POST",
    body: JSON.stringify(data)
  };

  if (token) {
    payload.headers.Authorization = `Bearer ${token}`;
  }
  return fetch(theBaseUrl + url, payload);
};

export const patchItem = (token, data, url, baseurl) => {
  let theBaseUrl = getBaseUrl(baseurl);
  // console.log(data);
  // console.log(theBaseUrl + url);
  return fetch(theBaseUrl + url, {
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    method: "PATCH",
    body: JSON.stringify(data)
  });
};

export const deleteItem = (token, id, url, baseurl) => {
  let theBaseUrl = getBaseUrl(baseurl);

  return fetch(theBaseUrl + url + "/" + id, {
    Authorization: `Bearer ${token}`,
    method: "DELETE"
  });
};
