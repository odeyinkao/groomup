import Booking from "../../models/booking";
import { getList, deleteItem, postItem, patchItem } from "../api";
import { Ionicons } from "@expo/vector-icons";

export const DELETE_BOOKING = "DELETE_BOOKING";
export const CREATE_BOOKING = "CREATE_BOOKING";
export const UPDATE_BOOKING = "UPDATE_BOOKING";
export const SET_BOOKINGS = "SET_BOOKINGS";
export const SET_CLIENT_BOOKINGS = "SET_CLIENT_BOOKINGS";
export const BOOK_BOOKING = "BOOK_BOOKING";

export const fetchBookingForClients = data => {
  return async (dispatch, getState) => {
    try {
      let { user } = getState().auth;
      // const providerId = getState().services.providerId;

      const response = await getList(user.token, `products/client/bookings`);

      if (!response.ok) {
        throw new Error("Something went wrong!");
      }

      const responseData = await response.json();
      console.log(responseData);
      const loadedBookings = [];

      for (const data of responseData) {
        loadedBookings.push(new Booking(data));
      }

      console.log(loadedBookings);
      dispatch({ type: SET_CLIENT_BOOKINGS, client_bookings: loadedBookings });
    } catch (err) {
      console.log(err);
      throw err;
    }
  };
};

export const fetchBookings = data => {
  return async (dispatch, getState) => {
    try {
      let { user } = getState().auth;

      const response = await getList(user.token, `products/provider/bookings`);

      // console.log(response);
      if (!response.ok) {
        throw new Error("Something went wrong!");
      }
      // console.log(response);

      const responseData = await response.json();
      const loadedBookings = [];

      for (const data of responseData) {
        loadedBookings.push(new Booking(data));
      }

      // console.log(loadedBookings);
      dispatch({ type: SET_BOOKINGS, bookings: loadedBookings });
    } catch (err) {
      console.log(err);
      throw err;
    }
  };
};

export const bookBooking = (id, serviceOwner) => {
  return async (dispatch, getState) => {
    try {
      let { user } = getState().auth;

      // const providerId = getState().services.providerId;

      // data.serviceProviderId = providerId;

      const response = await postItem(
        { id, "providerId": serviceOwner },
        `products/bookBooking`
      );

      // console.log(response);
      result = await response.json();

      // console.log(result);

      dispatch({
        type: BOOK_BOOKING,
        booked: result
      });
    } catch (err) {
      console.log(err);
      throw err;
    }
  };
};

export const deleteBooking = bookingId => {
  return async (dispatch, getState) => {
    try {
      let { user } = getState().auth;

      const response = await deleteItem(user.token, `products/providers/1`);
      console.log(response.data);

      if (!response.ok) {
        throw new Error("Something went wrong!");
      }

      result = await response.json();
      console.log(result);

      dispatch({ type: DELETE_BOOKING, pid: result });
    } catch (err) {
      console.log(err);
      throw err;
    }
  };
};

export const createBooking = data => {
  return async (dispatch, getState) => {
    try {
      let { user } = getState().auth;
      // any async code you want!
      const response = await postItem(user.token, data, `products/providers`);
      // console.log(response.data);

      result = await response.json();
      console.log(result);

      dispatch({
        type: CREATE_BOOKING,
        bookingData: new Booking(result)
      });
    } catch (err) {
      console.log(err);
      throw err;
    }
  };
};

export const updateBooking = (id, title, description, imageUrl) => {
  return async (dispatch, getState) => {
    try {
      let { user } = getState().auth;
      const response = await patchItem(user.token, data, `products/providers`);
      // console.log(response.data);

      if (!response.ok) {
        throw new Error("Something went wrong!");
      }

      result = await response.json();
      // console.log(result);

      dispatch({
        type: UPDATE_BOOKING,
        pid: id,
        bookingData: {
          ...result
        }
      });
    } catch (err) {
      console.log(err);
      throw err;
    }
  };
};
