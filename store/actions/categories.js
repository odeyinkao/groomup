import Category from "../../models/category";
import { getList, deleteItem, postItem, patchItem } from "../api";

export const SET_CATEGORIES = "SET_CATEGORIES";

export const fetchCategories = () => {
  return async (dispatch, getState) => {
    try {
      let { user } = getState().auth;

      const response = await getList(user.token, `products/categories`);
      if (!response.ok) {
        throw new Error("Something went wrong!");
      }

      // const responseData = JSON.stringify(response.json, null)
      //   ? await response.json()
      //   : null;

      const responseData = await response.json();
      const loadedCategories = [];

      for (const data of responseData) {
        loadedCategories.push(new Category(data));
      }

      dispatch({ type: SET_CATEGORIES, categories: loadedCategories });
    } catch (err) {
      throw err;
    }
  };
};
