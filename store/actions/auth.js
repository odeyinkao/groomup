import { getList, deleteItem, postItem, patchItem } from "../api";
import User from "../../models/user";
import * as categoriesActions from "../../store/actions/categories";
import { SET_FAVORITES } from "./services";

import { AsyncStorage } from "react-native";
// import Category from "../../models/category";
import Service from "../../models/service";
import { SizeClassIOS } from "expo/build/ScreenOrientation/ScreenOrientation";

export const SET_USER = "SET_USER";

export const setUserService = data => {
  return async dispatch => {
    try {
      let response = null;
      switch (data.sourceType) {
        case "Google":
          console.log(data);
          response = await postItem(null, data, `authenticate/gmail`);
          break;
        case "Facebook":
          response = await postItem(null, data, `authenticate/facebook`);
          break;
      }

      console.log(response);
      if (!response.ok) {
        console.log(response);
        throw new Error("Something went wrong!");
      }

      const userToken = await response.json();
      dispatch({
        type: SET_USER,
        user: { ...data, token: userToken.token }
      });

      saveDataToStorage(data);

      responseFavorite = await getList(
        userToken.token,
        `products/client/favorites`
      );

      if (!responseFavorite.ok) {
        throw new Error("Something went wrong!");
      }

      const responseData = await responseFavorite.json();
      const favoriteList = [];

      console.log(responseData);

      for (const data of responseData) {
        favoriteList.push(new Service(data));
      }

      console.log(favoriteList);
      dispatch({
        type: SET_FAVORITES,
        services: favoriteList
      });
    } catch (err) {
      throw err;
    }
  };
};

export const logoutService = serviceId => {
  return async (dispatch, getState) => {
    try {
      let { user } = getState().auth;
      const response = await deleteItem(user.token, `products/providers/1`);
      console.log(response.data);

      if (!response.ok) {
        throw new Error("Something went wrong!");
      }
      dispatch({ type: CLEAR_USER });
    } catch (err) {
      throw err;
    }
  };
};

const saveDataToStorage = user => {
  AsyncStorage.setItem("userData", JSON.stringify(user));
};
