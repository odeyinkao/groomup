import Service from "../../models/service";
import Schedule from "../../models/schedule";
import { getList, deleteItem, postItem, patchItem, getItem } from "../api";

export const DELETE_SERVICE = "DELETE_SERVICE";
export const CREATE_SERVICE = "CREATE_SERVICE";
export const UPDATE_SERVICE = "UPDATE_SERVICE";
export const SET_SERVICES = "SET_SERVICES";

export const SET_FAVORITES = "SET_FAVORITES";
export const ADD_FAVORITE = "ADD_FAVORITE";
export const REMOVE_FROM_FAVORITE = "REMOVE_FROM_FAVORITE";

export const SET_ADMIN_SERVICE = "SET_ADMIN_SERVICE";
export const EDIT_ADMIN_SERVICE = "EDIT_ADMIN_SERVICE";

export const SET_CATEGORY_ID = "SET_CATEGORY_ID";

export const fetchServices = data => {
  return async (dispatch, getState) => {
    try {
      let { user } = getState().auth;
      const response = await getList(
        user.token,
        `products/providers/` + data.categoryId
      ); //Todo: id should be from login

      if (!response.ok) {
        throw new Error("Something went wrong!");
      }

      const responseData = await response.json();
      const loadedServices = [];
      console.log(responseData);
      for (const data of responseData) {
        loadedServices.push(new Service(data));
      }

      dispatch({ type: SET_CATEGORY_ID, id: data.categoryId });
      dispatch({ type: SET_SERVICES, services: loadedServices });
    } catch (err) {
      throw err;
    }
  };
};

export const fetchMyService = () => {
  return async (dispatch, getState) => {
    try {
      let { user } = getState().auth;

      const response = await getItem(user.token, `products/provider/services`);

      if (!response.ok) {
        throw new Error("Something went wrong!");
      }

      let responseData = null;
      try {
        responseData = await response.json();
      } catch (e) {
        dispatch({ type: SET_ADMIN_SERVICE, service: null });
      }

      let schedules = [];
      if (responseData) {
        for (const data of responseData.schedules) {
          schedules.push(new Schedule(data));
        }

        const service = new Service(responseData);
        service.schedules = schedules;

        dispatch({ type: SET_ADMIN_SERVICE, service: service });
      } else {
        dispatch({ type: SET_ADMIN_SERVICE, service: null });
      }
    } catch (err) {
      console.log(err);
      throw err;
    }
  };
};

export const deleteService = serviceId => {
  return async (dispatch, getState) => {
    try {
      let { user } = getState().auth;
      const response = await deleteItem(user.token, `products/providers/1`);
      console.log(response.data);

      if (!response.ok) {
        throw new Error("Something went wrong!");
      }

      result = await response.json();
      console.log(result);

      dispatch({ type: DELETE_SERVICE, pid: serviceId });
    } catch (err) {
      throw err;
    }
  };
};

export const createService = data => {
  return async (dispatch, getState) => {
    try {
      let { user } = getState().auth;
      // any async code you want!
      const response = await postItem(
        user.token,
        data,
        `products/provider/details`
      );

      console.log(response.data);

      result = await response.json();
      console.log(result);

      let newSchedule = new Service(result);

      dispatch({
        type: CREATE_SERVICE,
        serviceData: newSchedule
      });
    } catch (err) {
      throw err;
    }
  };
};

export const updateService = data => {
  return async (dispatch, getState) => {
    try {
      let { user } = getState().auth;
      const response = await patchItem(
        user.token,
        data,
        `products/provider/details`
      );
      console.log(response);

      if (!response.ok) {
        throw new Error("Something went wrong!");
      }

      result = await response.json();
      console.log(result);

      let updatedSchedule = new Service(result);
      dispatch({
        type: UPDATE_SERVICE,
        serviceData: updatedSchedule
      });
    } catch (err) {
      throw err;
    }
  };
};

export const toggleFavorite = providerId => {
  return async (dispatch, getState) => {
    try {
      let { user } = getState().auth;
      let { favoriteServices } = getState().services;

      let response = null;
      console.log(providerId);
      console.log(favoriteServices);
      let isExiting = favoriteServices.find(item => {
        if (item.id === providerId) return item;
      });

      // console.log(favoriteServices);
      // console.log(providerId);
      console.log(isExiting);

      if (isExiting) {
        console.log("Up");
        console.log(user.token);
        response = await deleteItem(
          user.token,
          isExiting.id,
          `products/client/favorites`
        );
      } else {
        console.log("Here");

        response = await getItem(
          user.token,
          `products/client/favorites`,
          providerId
        );
      }

      console.log(response);

      if (!response.ok) {
        throw new Error("Something went wrong!");
      }

      result = await response.json();
      console.log(result);

      // console.log(mySchedule);
      if (isExiting) {
        dispatch({
          type: REMOVE_FROM_FAVORITE,
          data: isExiting
        });
      } else {
        let mySchedule = new Service(result);
        dispatch({
          type: ADD_FAVORITE,
          data: mySchedule
        });
      }
    } catch (err) {
      throw err;
    }
  };
};
