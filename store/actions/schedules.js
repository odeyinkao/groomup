import Schedule from "../../models/schedule";
import moment from "moment";
import { getList, deleteItem, postItem, patchItem } from "../api";

export const DELETE_SCHEDULE = "DELETE_SCHEDULE";
export const CREATE_SCHEDULE = "CREATE_SCHEDULE";
export const UPDATE_SCHEDULE = "UPDATE_SCHEDULE";
export const SET_SCHEDULES = "SET_SCHEDULES";
export const SET_SERVICE_ID = "SET_SERVICE_ID";
export const BOOK_SCHEDULE = "BOOK_SCHEDULE";

export const fetchSchedules = data => {
  return async (dispatch, getState) => {
    try {
      let { user } = getState().auth;
      const categoryId = getState().services.categoryId;
      console.log(categoryId);
      console.log(data);
      const response = await getList(
        user.token,
        `products/schedules/` + data.serviceId + "/" + categoryId
      );

      if (!response.ok) {
        throw new Error("Something went wrong!");
      }

      const responseData = await response.json();
      const loadedSchedules = [];

      for (const data of responseData) {
        loadedSchedules.push(new Schedule(data));
      }

      // console.log(loadedSchedules);
      dispatch({ type: SET_SERVICE_ID, id: data.serviceId });
      dispatch({ type: SET_SCHEDULES, schedules: loadedSchedules });
    } catch (err) {
      console.log(err);
      throw err;
    }
  };
};

export const bookSchedule = (id, serviceOwner) => {
  return async (dispatch, getState) => {
    try {
      let { user } = getState().auth;
      console.log(id);
      console.log(serviceOwner);
      // const providerId = getState().services.providerId;
      // data.serviceProviderId = providerId;

      const response = await postItem(
        user.token,
        { id, providerId: serviceOwner },
        `products/bookSchedule`
      );

      // console.log(response);
      result = await response.json();

      // console.log(result);

      dispatch({
        type: BOOK_SCHEDULE,
        booked: result
      });
    } catch (err) {
      console.log(err);
      throw err;
    }
  };
};

export const deleteSchedule = scheduleId => {
  return async (dispatch, getState) => {
    try {
      let { user } = getState().auth;

      const response = await deleteItem(user.token, `products/providers/1`);
      console.log(response.data);

      if (!response.ok) {
        throw new Error("Something went wrong!");
      }

      result = await response.json();
      console.log(result);

      dispatch({ type: DELETE_SCHEDULE, pid: result });
    } catch (err) {
      console.log(err);
      throw err;
    }
  };
};

export const createSchedule = (start, end, status, categoryId) => {
  startDate = moment(start);
  endDate = moment(end);
  return async (dispatch, getState) => {
    try {
      let { user } = getState().auth;
      // any async code you want!
      console.log("Here Now");
      // console.log({ start, end, status, categoryId });
      const response = await postItem(
        user.token,
        {
          // id: 0,
          period: { start: startDate, end: endDate },
          status,
          categoryId,
          serviceProviderId: ""
        },
        `products/service/schedule`
      );
      console.log(response);
      // console.log(response.data);

      // let result = await response.json();
      // console.log(result);

      let result = await response.json();
      console.log(result);
      let newSchedule = new Schedule(result);
      console.log(newSchedule);

      dispatch({
        type: CREATE_SCHEDULE,
        scheduleData: newSchedule
      });
    } catch (err) {
      console.log(err);
      throw err;
    }
  };
};

export const updateSchedule = (id, start, end, status, categoryId) => {
  startDate = moment(start);
  endDate = moment(end);
  // console.log({
  //   id,
  //   period: { start: startDate, end: endDate },
  //   status,
  //   categoryId,
  //   serviceProviderId: 0
  // });
  return async (dispatch, getState) => {
    try {
      let { user } = getState().auth;

      const response = await patchItem(
        user.token,
        {
          id,
          period: { start: startDate, end: endDate },
          status,
          categoryId,
          serviceProviderId: ""
        },
        `products/service/schedule`
      );

      if (!response.ok) {
        throw new Error("Something went wrong!");
      }

      result = await response.json();
      console.log(result);
      let updatedSchedule = new Schedule(result);
      console.log(updatedSchedule);
      dispatch({
        type: UPDATE_SCHEDULE,
        pid: updatedSchedule.id,
        scheduleData: {
          ...updatedSchedule
        }
      });
    } catch (err) {
      console.log(err);
      throw err;
    }
  };
};
