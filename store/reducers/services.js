import {
  DELETE_SERVICE,
  CREATE_SERVICE,
  UPDATE_SERVICE,
  SET_SERVICES,
  SET_FAVORITES,
  ADD_FAVORITE,
  REMOVE_FROM_FAVORITE,
  SET_ADMIN_SERVICE,
  ADMIN_UPDATE_SCHEDULE,
  ADMIN_CREATE_SCHEDULE,
  ADMIN_DELETE_SCHEDULE,
  ADMIN_UPDATE_SERVICE,
  // ADMIN_UPDATE_SERVICE
  SET_CATEGORY_ID
} from "../actions/services";
import Service from "../../models/service";

const initialState = {
  favoriteServices: [],
  availableServices: [],
  userService: null,
  categoryId: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_SERVICES:
      return {
        ...state,
        availableServices: action.services
      };
    case SET_FAVORITES:
      let allFavoriteServices = action.services;
      console.log(allFavoriteServices);
      if (!(allFavoriteServices && allFavoriteServices.length > 0)) {
        allFavoriteServices = [];
      }
      return {
        ...state,
        favoriteServices: [...allFavoriteServices]
      };
    case CREATE_SERVICE:
      return {
        ...state,
        userService: { ...action.serviceData }
      };

    case ADD_FAVORITE:
      const favorites = state.favoriteServices ? state.favoriteServices : [];
      return {
        ...state,
        favoriteServices: [...favorites, action.data]
      };
    case REMOVE_FROM_FAVORITE:
      const favoritesExiting = state.favoriteServices
        ? state.favoriteServices
        : [];
      return {
        ...state,
        favoriteServices: favoritesExiting.filter(
          item => item.id !== action.data.id
        )
      };

    case UPDATE_SERVICE:
      console.log(action.serviceData);
      return {
        ...state,
        userService: {
          ...action.serviceData,
          schedules: [...state.userService.schedules]
        }
      };

    case DELETE_SERVICE:
      return {
        ...state,
        availableServices: state.availableServices.filter(
          service => service.id !== action.pid
        )
      };

    case SET_ADMIN_SERVICE:
      // console.log(action.service);
      let newState = {
        ...state,
        userService: action.service
      };
      // console.log(action.service);
      return newState;

    case SET_CATEGORY_ID:
      return {
        ...state,
        categoryId: action.id
      };

    case ADMIN_CREATE_SCHEDULE:
      const newSchedule = new Schedule(action.schedules);
      return {
        ...state,
        userService: {
          ...state.userService,
          schedules: [...state.schedules, newSchedule]
        }
      };

    case ADMIN_UPDATE_SCHEDULE:
      const scheduleIndex = state.userService.schedules.findIndex(
        prod => prod.id === action.pid
      );
      const updatedSchedule = new Schedule({
        ...action.scheduleData,
        id: action.pid, //To be deleted  from sent data
        userId: state.userService.schedules[scheduleIndex].ownerId //To be deleted  from sent data
      });
      const updatedSchedules = [...state.userService.schedules];
      updatedSchedules[scheduleIndex] = updatedSchedule;

      return {
        ...state,
        userService: { ...state.userService, schedules: updatedSchedules }
      };
  }
  return state;
};
