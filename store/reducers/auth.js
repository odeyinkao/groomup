import { SET_USER, CLEAR_USER } from "../actions/auth";
import User from "../../models/user";

const initialState = { user: new User() };

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_USER:
      // console.log({ here: { ...action.user } });
      return { user: action.user };
    case CLEAR_USER:
      return { user: new User() };
    default:
      return state;
  }
};
