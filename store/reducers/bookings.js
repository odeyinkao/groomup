import {
  DELETE_BOOKING,
  CREATE_BOOKING,
  UPDATE_BOOKING,
  SET_BOOKINGS,
  SET_CLIENT_BOOKINGS
} from "../actions/bookings";
import Booking from "../../models/booking";

const initialState = {
  bookings: [],
  client_bookings: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_BOOKINGS:
      return {
        ...state,
        bookings: action.bookings
      };
    case SET_CLIENT_BOOKINGS:
      return {
        ...state,
        client_bookings: action.client_bookings
      };

    case CREATE_BOOKING:
      const newBooking = new Booking(action.bookings);
      return {
        ...state,
        bookings: [...state.bookings, newBooking]
      };

    case UPDATE_BOOKING:
      const bookingIndex = state.bookings.findIndex(
        prod => prod.id === action.pid
      );
      const updatedBooking = new Booking({
        ...action.bookingData,
        id: action.pid, //To be deleted  from sent data
        userId: state.bookings[bookingIndex].ownerId //To be deleted  from sent data
      });
      const updatedBookings = [...state.bookings];
      updatedBookings[bookingIndex] = updatedBooking;

      return {
        ...state,
        bookings: updatedBookings
      };

    case DELETE_BOOKING:
      return {
        ...state,
        bookings: state.bookings.filter(booking => booking.id !== action.pid)
      };
  }
  return state;
};
