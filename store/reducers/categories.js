import { SET_CATEGORIES } from "../actions/categories";
import Category from "../../models/category";

const initialState = {
  categories: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_CATEGORIES:
      return {
        ...state,
        categories: action.categories
      };
  }
  return state;
};
