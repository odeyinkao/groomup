import {
  DELETE_SCHEDULE,

  SET_SCHEDULES,
  SET_SERVICE_ID,
  BOOK_SCHEDULE
} from "../actions/schedules";
import Schedule from "../../models/schedule";

const initialState = {
  schedules: [],
  serviceId: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_SCHEDULES:
      return {
        ...state,
        schedules: action.schedules
      };
    

    case DELETE_SCHEDULE:
      return {
        ...state,
        schedules: state.schedules.filter(
          schedule => schedule.id !== action.pid
        )
      };

    case SET_SERVICE_ID:
      return {
        ...state,
        serviceId: action.id
      };

    case BOOK_SCHEDULE:
      console.log(action.booked);
      const bookedIndex = state.schedules.findIndex(
        prod => prod.id === action.booked.id
      );

      console.log(bookedIndex);
      console.log("Here there")
      const updatedBooked = new Schedule({
        ...action.booked,
        id: action.booked.id, //To be deleted  from sent data
        serviceProviderId: state.schedules[bookedIndex].serviceProviderId //To be deleted  from sent data
      });
      console.log("Here there jj")

      const updateBookSchedules = [...state.schedules];
      updateBookSchedules[bookedIndex] = updatedBooked;
      console.log(updateBookSchedules);
      return {
        ...state,
        schedules: updateBookSchedules
      };
  }
  return state;
};
