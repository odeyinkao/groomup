import React, { useReducer, useEffect } from "react";
import {
  View,
  Text,
  Button,
  DatePickerIOS,
  StyleSheet,
  TextInput,
  Platform,
  TimePickerAndroid,
  DatePickerAndroid
} from "react-native";
import moment from "moment";

const INPUT_CHANGE = "INPUT_CHANGE";
const INPUT_BLUR = "INPUT_BLUR";

const dateInputReducer = (state, action) => {
  // console.log(action.type + " Tomorrow ");
  switch (action.type) {
    case INPUT_CHANGE:
      return {
        ...state,
        value: action.value
      };
    case INPUT_BLUR:
      return {
        ...state,
        touched: true
      };
    default:
      return state;
  }
};

const DateTimeInput = props => {
  const [inputState, dispatch] = useReducer(dateInputReducer, {
    value: props.initialValue,
    isValid: true,
    touched: false
  });

  const { onInputChange, id } = props;

  useEffect(() => {
    onInputChange(id, inputState.value, true);
  }, [inputState, onInputChange, id]);

  const textChangeHandler = (text, id) => {
    dispatch({ type: INPUT_CHANGE, value: text, isValid: true });
  };

  const textChangeHandlerAndroid = (text, id) => {
    dispatch({ type: INPUT_CHANGE, value: text, id: id, isValid: true });
    onInputChange(id, text, true);
  };

  const lostFocusHandler = () => {
    dispatch({ type: INPUT_BLUR });
  };

  const datePicker = async () => {
    try {
      let timeValue = inputState.value;
      // var afterTomorrow = timeValue.setDate(timeValue.getDate() + 2);
      const { action, year, month, day } = await DatePickerAndroid.open({
        date: timeValue,
        // date: new Date(),
        // minDate: new Date(),
        // maxDate: afterTomorrow,
        mode: "calendar"
      });
      if (action !== DatePickerAndroid.dismissedAction) {
        timePicker(day, month, year, id);
      }
    } catch ({ code, message }) {
      alert("Cannot open date picker: " + message);
    }
  };

  const timePicker = async (day, month, year, id) => {
    const TimePickerModule = require("NativeModules").TimePickerAndroid;
    let timeValue = inputState.value;
    try {
      const { action, hour, minute } = await TimePickerAndroid.open({
        hour: timeValue.getHours(),
        minute: timeValue.getMinutes(),
        is24Hour: false // Will display '2 PM'
      });
      if (action !== TimePickerAndroid.dismissedAction) {
        // Selected hour (0-23), minute (0-59)
        //Applying extra 0 before the hour/minute for better visibility
        // 9 minutes => 09 minutes
        var m = minute < 10 ? "0" + minute : minute;
        var h = hour < 10 ? "0" + hour : hour;
        let pickedDateTimeString =
          year + "-" + month + "-" + day + " " + h + ":" + m;

        let pickedDateTime = moment(
          pickedDateTimeString,
          "YYYY-MM-DD HH:mm"
        ).toDate();

        textChangeHandlerAndroid(pickedDateTime, id);
      }
    } catch ({ code, message }) {
      alert("Cannot open time picker" + message);
    }
  };

  return (
    <View style={styles.formControl}>
      <Text style={styles.label}>{props.label}</Text>

      {Platform.OS === "android" ? (
        <View style={styles.container}>
          <TextInput
            {...props}
            style={styles.input}
            value={moment(inputState.value).format("YYYY-MM-DD HH:mm")}
            editable={false}
            // onChangeText={textChangeHandler}
            // onBlur={lostFocusHandler}
          />
          <Button
            {...props}
            onPress={datePicker}
            title="Pick Date"
            color={Colors.primaryColor}
          />
        </View>
      ) : (
        <View style={styles.container}>
          <DatePickerIOS
            {...props}
            style={styles.input}
            date={inputState.value}
            onDateChange={textChangeHandler}
          />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  formControl: {
    width: "100%"
  },
  label: {
    fontFamily: "open-sans-bold",
    marginVertical: 8
  },
  input: {
    paddingHorizontal: 2,
    paddingVertical: 5,
    borderBottomColor: "#ccc",
    borderBottomWidth: 1
  },
  errorContainer: {
    marginVertical: 5
  },
  errorText: {
    fontFamily: "open-sans",
    color: "red",
    fontSize: 13
  }
});

export default DateTimeInput;
