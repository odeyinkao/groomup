import React from "react";
import { View, StyleSheet } from "react-native";

const HorizontalLine = props => {
  return (
    <View
      style={{
        borderBottomColor: "black",
        borderBottomWidth: StyleSheet.hairlineWidth,
        width: '100%',
        ...props.style
      }}
    />
  );
};

export default HorizontalLine;
