import React, { useReducer, useEffect } from "react";
import { View, Text, Switch, StyleSheet } from "react-native";

import Colors from "../../constants/Colors";

const INPUT_CHANGE = "INPUT_CHANGE";
const INPUT_BLUR = "INPUT_BLUR";

const switchReducer = (state, action) => {
  switch (action.type) {
    case INPUT_CHANGE:
      return {
        ...state,
        value: action.value
      };
    case INPUT_BLUR:
      return {
        ...state,
        touched: true
      };
    default:
      return state;
  }
};

const SwitchInput = props => {
  const [inputState, dispatch] = useReducer(switchReducer, {
    value: props.initialValue ? props.initialValue : false,
    isValid: true,
    touched: false
  });

  const { onInputChange, id } = props;

  useEffect(() => {
    onInputChange(id, inputState.value, true);
  }, [inputState, onInputChange, id]);

  const textChangeHandler = text => {
    console.log(text);
    dispatch({ type: INPUT_CHANGE, value: text, isValid: true });
  };

  const lostFocusHandler = () => {
    dispatch({ type: INPUT_BLUR });
  };

  return (
    <View style={styles.filterContainer}>
      <Text>{props.label}</Text>
      <Switch
        trackColor={{ true: Colors.primaryColor }}
        {...props}
        value={inputState.value}
        onValueChange={textChangeHandler}
        onBlur={lostFocusHandler}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  formControl: {
    width: "100%"
  },
  label: {
    fontFamily: "open-sans-bold",
    marginVertical: 8
  },
  input: {
    paddingHorizontal: 2,
    paddingVertical: 5,
    borderBottomColor: "#ccc",
    borderBottomWidth: 1
  },
  errorContainer: {
    marginVertical: 5
  },
  errorText: {
    fontFamily: "open-sans",
    color: "red",
    fontSize: 13
  },
  filterContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginVertical: 15,
    width: "80%"
  }
});

export default SwitchInput;
