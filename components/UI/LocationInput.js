import React, { useReducer, useEffect } from "react";
import { Platform, Text, View, Button, StyleSheet } from "react-native";
import Constants from "expo-constants";
import * as Location from "expo-location";
import * as Permissions from "expo-permissions";

import Colors from "../../constants/Colors";

const INPUT_CHANGE = "INPUT_CHANGE";
// const INPUT_BLUR = "INPUT_BLUR";
const INPUT_ERROR = "INPUT_ERROR";

const locationReducer = (state, action) => {
  switch (action.type) {
    case INPUT_CHANGE:
      return {
        ...state,
        isValid: action.isValid,
        error: "",
        ...action.value.coords
      };
    // case INPUT_BLUR:
    //   return {
    //     ...state,
    //     touched: true
    //   };
    case INPUT_ERROR:
      return {
        ...state,
        error: action.error
      };
    default:
      return state;
  }
};

const LocationInput = props => {
  const [inputState, dispatch] = useReducer(locationReducer, {
    value: props.initialValue ? props.initialValue : {},
    error: "You must set location by clicking link above",
    isValid: true,
    touched: false
  });

  const { onLocationSet, id } = props;

  useEffect(() => {
    // console.log(inputState.value);

    onLocationSet(
      id,
      {
        accuracy: inputState.accuracy,
        altitude: inputState.altitude,
        altitudeAccuracy: inputState.altitudeAccuracy,
        heading: inputState.heading,
        latitude: inputState.latitude,
        longitude: inputState.longitude,
        speed: inputState.speed
      },
      true
    );
  }, [inputState, onLocationSet]);

  setHereAsLocation = () => {
    if (Platform.OS === "android" && !Constants.isDevice) {
      dispatch({
        type: INPUT_ERROR,
        error:
          "Oops, this will not work on Sketch in an Android emulator. Try it on your device!"
      });
    } else {
      // console.log("Here Now");
      return _getLocationAsync();
    }
  };

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== "granted") {
      dispatch({
        type: INPUT_ERROR,
        error: "Permission to access location was denied"
      });
    }

    // console.log("Here Location");
    let location = await Location.getCurrentPositionAsync({});
    dispatch({ type: INPUT_CHANGE, value: location, isValid: true });
  };

  async function getLocationAsync() {
    // permissions returns only for location permissions on iOS and under certain conditions, see Permissions.LOCATION
    const { status, permissions } = await Permissions.askAsync(
      Permissions.LOCATION
    );
    if (status === "granted") {
      return Location.getCurrentPositionAsync({ enableHighAccuracy: true });
    } else {
      throw new Error("Location permission not granted");
    }
  }

  // const textChangeHandler = text => {
  //   console.log(text);
  //   dispatch({ type: INPUT_CHANGE, value: text, isValid: true });
  // };

  // const lostFocusHandler = () => {
  //   dispatch({ type: INPUT_BLUR });
  // };

  return (
    <View style={{ flexDirection: "column" }}>
      <View style={styles.filterContainer}>
        <Text style={styles.label}>{props.label}</Text>
        <Button
          onPress={setHereAsLocation}
          title="Set Here As Location"
          color={Colors.primaryColor}
        />
      </View>
      {inputState.error !== "" ? (
        <Text style={{ ...styles.input, color: "red" }}>
          {inputState.error}
        </Text>
      ) : null}
      {inputState.latitude ? (
        <View style={styles.formControl}>
          <Text style={styles.label}>Latitude</Text>
          <Text style={styles.input}>{inputState && inputState.latitude}</Text>
        </View>
      ) : null}
      {inputState.longitude ? (
        <View style={styles.formControl}>
          <Text style={styles.label}>Longitude</Text>
          <Text style={styles.input}>{inputState && inputState.longitude}</Text>
        </View>
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  formControl: {
    flexDirection: "row"
  },
  label: {
    fontFamily: "open-sans-bold",
    marginVertical: 8
  },
  input: {
    paddingHorizontal: 2,
    paddingVertical: 5,
    borderBottomColor: "#ccc",
    borderBottomWidth: 1
  },
  errorContainer: {
    marginVertical: 5
  },
  errorText: {
    fontFamily: "open-sans",
    color: "red",
    fontSize: 13
  },
  filterContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginVertical: 15,
    width: "80%"
  }
});

export default LocationInput;
