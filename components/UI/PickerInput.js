import React, { useReducer, useEffect } from "react";
import { View, Text, Picker, StyleSheet } from "react-native";

const INPUT_CHANGE = "INPUT_CHANGE";
const INPUT_BLUR = "INPUT_BLUR";

const pickerReducer = (state, action) => {
  switch (action.type) {
    case INPUT_CHANGE:
      return {
        ...state,
        value: action.value
      };
    case INPUT_BLUR:
      return {
        ...state,
        touched: true
      };
    default:
      return state;
  }
};

const PickerInput = props => {
  const [inputState, dispatch] = useReducer(pickerReducer, {
    value: props.initialValue ? props.initialValue : "",
    isValid: true,
    touched: false
  });

  const { onInputChange, id } = props;

  useEffect(() => {
    onInputChange(id, inputState.value, true);
  }, [inputState, onInputChange, id]);

  const textChangeHandler = text => {
    console.log(text);
    dispatch({ type: INPUT_CHANGE, value: text, isValid: true });
  };

  const lostFocusHandler = () => {
    dispatch({ type: INPUT_BLUR });
  };

  return (
    <View style={styles.formControl}>
      <Text style={styles.label}>{props.label}</Text>
      <Picker
        {...props}
        style={styles.input}
        onValueChange={textChangeHandler}
        selectedValue={inputState.value}
        onBlur={lostFocusHandler}
      >
        {props.pickerItems.map(pickerItem => (
          <Picker.Item
            key={pickerItem.value}
            label={pickerItem.label}
            value={pickerItem.value}
          />
        ))}
      </Picker>
    </View>
  );
};

const styles = StyleSheet.create({
  formControl: {
    width: "100%"
  },
  label: {
    fontFamily: "open-sans-bold",
    marginVertical: 8
  },
  input: {
    paddingHorizontal: 2,
    paddingVertical: 5,
    borderBottomColor: "#ccc",
    borderBottomWidth: 1
  },
  errorContainer: {
    marginVertical: 5
  },
  errorText: {
    fontFamily: "open-sans",
    color: "red",
    fontSize: 13
  }
});

export default PickerInput;
