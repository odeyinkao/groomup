import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ImageBackground
} from "react-native";
import DefaultText from "../UI/DefaultText";
import Images from "../../constants/LocalImageCollection";

const ServiceItem = props => {
  return (
    <View style={styles.serviceItem}>
      <TouchableOpacity onPress={props.onSelectedService}>
        <View>
          <View style={{ ...styles.serviceRow, ...styles.serviceHeader }}>
            <ImageBackground
              //source={{ uri: props.image }}
              source={Images[props.image]}
              // source={require("../assets/images/salon/01.jpg")}
              style={styles.bgImage}
            >
              <View style={styles.titleContainer}>
                <Text style={styles.title} numberOfLines={1}>
                  {props.title}
                </Text>
              </View>
            </ImageBackground>
          </View>
          <View style={{ ...styles.serviceRow, ...styles.serviceDetail }}>
            <DefaultText>{props.address.toUpperCase()}</DefaultText>
            <DefaultText>{props.phoneNo.toUpperCase()}</DefaultText>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  serviceItem: {
    height: 200,
    width: "100%",
    backgroundColor: "#f5f5f5",
    borderRadius: 10,
    overflow: "hidden"
  },
  serviceRow: {
    flexDirection: "row"
  },
  serviceHeader: {
    height: "85%"
  },
  serviceDetail: {
    paddingHorizontal: 10,
    justifyContent: "space-between",
    alignItems: "center",
    height: "15%"
  },
  bgImage: {
    width: "100%",
    height: "100%",
    justifyContent: "flex-end"
  },
  titleContainer: {
    backgroundColor: "rgba(0,0,0,0.4)",
    paddingHorizontal: 12,
    paddingVertical: 5
  },
  title: {
    fontSize: 22,
    fontFamily: "open-sans-bold",
    color: "white",
    textAlign: "center"
  }
});

export default ServiceItem;
