import React from "react";
import { View, FlatList, StyleSheet } from "react-native";
import { useSelector } from "react-redux";

import ServiceItem from "./ServiceItem";

const ServiceList = props => {
  const favoriteServices = useSelector(
    state => state.services.favoriteServices
  );

  const renderServiceItem = itemData => {
    console.log(favoriteServices);
    const isFavorite = favoriteServices
      ? favoriteServices.some(service => service.id === itemData.item.id)
      : false;
    return (
      <ServiceItem
        title={itemData.item.title}
        image={itemData.item.imageUrl}
        address={itemData.item.address}
        phoneNo={itemData.item.phoneNo}
        onSelectedService={() => {
          props.navigation.navigate({
            routeName: "ServiceDetail",
            params: {
              serviceId: itemData.item.id,
              serviceTitle: itemData.item.title,
              isFavorite: isFavorite
            }
          });
        }}
      />
    );
  };

  return (
    <View style={styles.screen}>
      <FlatList
        data={props.listData}
        keyExtractor={(item, index) => item.id}
        renderItem={renderServiceItem}
        style={{ width: "100%" }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    padding: 10
  }
});

export default ServiceList;
