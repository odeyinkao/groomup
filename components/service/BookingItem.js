import React, { useState } from "react";
import { View, Text, Button, StyleSheet } from "react-native";

// import ScheduleItem from "./ScheduleItem";
import DefaultText from "../UI/DefaultText";
import Colors from "../../constants/Colors";
import Card from "../UI/Card";
import moment from "moment";

const BookingItem = props => {
  var start = moment(props.item.start);
  var end = moment(props.item.start);
  return (
    <Card style={styles.bookingItem}>
      <View style={styles.summary}>
        <Text style={styles.amount}>
          ${props.item.amount ? props.item.amount.toFixed(2) : ""}
        </Text>
        <Text style={styles.date}>{props.date || ""}</Text>
      </View>

      <DefaultText style={styles.stateText}>{props.item.status}</DefaultText>
      <DefaultText>
        {start.format("dddd, Do-MMM-YY, HH:mm")} - {end.format("HH:mm")}
      </DefaultText>
    </Card>
  );
};

const styles = StyleSheet.create({
  bookingItem: {
    margin: 20,
    padding: 10,
    alignItems: "center"
  },
  summary: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    marginBottom: 15
  },
  totalAmount: {
    fontFamily: "open-sans-bold",
    fontSize: 16
  },
  date: {
    fontSize: 16,
    fontFamily: "open-sans",
    color: "#888"
  },
  detailItems: {
    width: "100%"
  }
});

export default BookingItem;
