import React from "react";
import { View, FlatList, StyleSheet, Button } from "react-native";
import { useSelector } from "react-redux";

import Card from "../UI/Card";
import DefaultText from "../UI/DefaultText";
import moment from "moment";

const ScheduleItem = props => {
  var start = moment(props.item.start);
  var end = moment(props.item.end);
  return (
    // <View style={styles.screen}>
    <Card style={styles.ScheduleItem}>
      <DefaultText style={styles.stateText}>{props.item.status}</DefaultText>
      <DefaultText>
        {start.format("ddd, Do-MMM-YY, HH:mm")} - {end.format("HH:mm")}
      </DefaultText>
      <Button
        title={props.actionLabel}
        onPress={() => props.actionFunction(props.item.id, props.serviceOwner)}
        title={
          props.item.status === props.actionVisibleStatus
            ? props.actionLabel
            : ""
        }
      />
    </Card>
    // </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    margin: 10
    // flex: 1
    // justifyContent: "center",
    // alignItems: "center",
    // padding: 10
  },
  ScheduleItem: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 10,
    marginLeft: 10,
    marginRight: 10,
    padding: 10,

    shadowRadius: 4,
    borderRadius: 5
  },
  stateText: {
    fontFamily: "open-sans-bold",
    fontSize: 18
  }
  // ScheduleItem: {
  // marginVertical: 10,
  // marginHorizontal: 20,
  // borderColor: "#ccc",
  // borderWidth: 1,
  // padding: 10,
  // flexDirection: "row",
  // justifyContent: "space-between",
  // alignItems: "center"
  // }
});

export default ScheduleItem;
