export default Images = {
  "01jpg": require("../assets/images/salon/01.jpg"),
  "02jpg": require("../assets/images/salon/02.jpg"),
  "03jpg": require("../assets/images/salon/03.jpg"),
  "04jpg": require("../assets/images/salon/04.jpg"),
  "05jpg": require("../assets/images/salon/05.jpg"),
  "06jpg": require("../assets/images/salon/06.jpg"),
  "07jpg": require("../assets/images/salon/07.jpg"),
  "08jpg": require("../assets/images/salon/08.jpg"),
  "09jpg": require("../assets/images/salon/09.jpg"),
  "10jpg": require("../assets/images/salon/10.jpg")
};
