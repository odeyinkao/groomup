export default {
  primaryColor: "#000080",
  accentColor: "#f5428d",
  google: "#db3236",
  facebook: "#3b5998"
};
