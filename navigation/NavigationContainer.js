import React, { useEffect, useRef } from "react";
import { useSelector } from "react-redux";
import { NavigationActions } from "react-navigation";

import GroomUpNavigation from "./GroomUpNavigation";

const NavigationContainer = props => {
  const navRef = useRef();
  
  const isAuth = useSelector(state => state.auth.user.token);

  useEffect(() => {
    if (!isAuth) {
      navRef.current.dispatch(
        NavigationActions.navigate({ routeName: "Auth" })
      );
    } // need to rework this block to have auto login
    else {
      navRef.current.dispatch(
        NavigationActions.navigate({ routeName: "GroomUp" })
      );
    }
  }, [isAuth]);

  return <GroomUpNavigation ref={navRef} />;
};

export default NavigationContainer;
