import React from "react";
import { Platform, Text, View, SafeAreaView, Button } from "react-native";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import { useDispatch } from "react-redux";
import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";

import { createAppContainer, createSwitchNavigator } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { createBottomTabNavigator } from "react-navigation-tabs";
import { createDrawerNavigator, DrawerItems } from "react-navigation-drawer";

import CategoriesScreen from "../screens/user/CategoriesScreen";
import ServicesScreen from "../screens/user/ServicesScreen";
import ServiceDetailScreen from "../screens/user/ServiceDetailScreen";
import FavoritesScreen from "../screens/user/FavoritesScreen";
import FiltersScreen from "../screens/user/FiltersScreen";

import AuthScreen from "../screens/user/AuthScreen";

import UserServicesScreen from "../screens/admin/UserServiceScreen";
import EditServiceScreen from "../screens/admin/EditServiceScreen";
import EditScheduleScreen from "../screens/admin/EditScheduleScreen";
import BookingsScreen from "../screens/user/BookingsScreen";
import StartupScreen from "../screens/StartupScreen";

import Colors from "../constants/Colors";

const defaultStackNavOptions = {
  headerStyle: {
    backgroundColor: Platform.OS === "android" ? Colors.primaryColor : "",
    fontFamily: "open-sans-bold"
  },
  headerBackTitleStyle: {
    fontFamily: "open-sans"
  },
  headerTintColor: Platform.OS === "android" ? "white" : Colors.primaryColor
};

const ServicesNavigator = createStackNavigator(
  {
    Categories: CategoriesScreen,
    CategoryServices: ServicesScreen,
    ServiceDetail: ServiceDetailScreen //long for that allow setting other screen properties
  },
  {
    defaultNavigationOptions: defaultStackNavOptions
  }
);

const FavNavigator = createStackNavigator(
  {
    Favorites: FavoritesScreen,
    ServiceDetail: ServiceDetailScreen
  },
  {
    defaultNavigationOptions: defaultStackNavOptions
  }
);

const tabScreenConfig = {
  Services: {
    screen: ServicesNavigator,
    navigationOptions: {
      tabBarIcon: tabInfo => {
        return (
          <MaterialCommunityIcons
            name="sign-direction"
            size={25}
            color={tabInfo.tintColor}
          />
        );
      },
      tabBarColor: Colors.primaryColor,
      tabBarLabel:
        Platform.OS === "android" ? (
          <Text style={{ fontFamily: "open-sans-bold" }}>Services</Text>
        ) : (
          "Services"
        )
    }
  },
  Favorites: {
    screen: FavNavigator,
    navigationOptions: {
      tabBarLabel: "Favorites!!!",
      tabBarIcon: tabInfo => {
        return <Ionicons name="ios-star" size={25} color={tabInfo.tintColor} />;
      },
      tabBarColor: Colors.accentColor,
      tabBarLabel:
        Platform.OS === "android" ? (
          <Text style={{ fontFamily: "open-sans-bold" }}>Favorites</Text>
        ) : (
          "Favorites"
        )
    }
  }
};

const ServicesFavTabNavigator =
  Platform.OS === "android"
    ? createMaterialBottomTabNavigator(tabScreenConfig, {
        activeTintColor: "white",
        shifting: true,
        // shifting: false
        barStyle: {
          backgroundColor: Colors.primaryColor
        }
      })
    : createBottomTabNavigator(tabScreenConfig, {
        tabBarOptions: {
          activeTintColor: Colors.accentColor,
          labelStyle: {
            fontFamily: "open-sans-bold"
          }
        }
      });

const FiltersNavigator = createStackNavigator(
  {
    Filters: FiltersScreen
  },
  {
    navigationOptions: {
      drawerIcon: drawerConfig => (
        <Ionicons
          name={Platform.OS === "android" ? "md-color-filter" : "ios-funnel"}
          size={23}
          color={drawerConfig.tintColor}
        />
      )
    },
    // navigationOptions: { drawerLabel: "Filters!!!" },
    defaultNavigationOptions: defaultStackNavOptions
  }
);

const BookingsNavigator = createStackNavigator(
  {
    Bookings: BookingsScreen
  },
  {
    navigationOptions: {
      drawerIcon: drawerConfig => (
        <Ionicons
          name={Platform.OS === "android" ? "md-list" : "ios-list"}
          size={23}
          color={drawerConfig.tintColor}
        />
      )
    },
    defaultNavigationOptions: defaultStackNavOptions
  }
);

const AdminNavigator = createStackNavigator(
  {
    UserServices: UserServicesScreen,
    EditService: EditServiceScreen,
    EditSchedule: EditScheduleScreen
  },
  {
    navigationOptions: {
      drawerIcon: drawerConfig => (
        <Ionicons
          name={Platform.OS === "android" ? "md-create" : "ios-create"}
          size={23}
          color={drawerConfig.tintColor}
        />
      )
    },
    defaultNavigationOptions: defaultStackNavOptions
  }
);

const GroomUpNavigator = createDrawerNavigator(
  {
    ServicesFav: {
      screen: ServicesFavTabNavigator,
      navigationOptions: {
        drawerLabel: "Services",
        drawerIcon: drawerConfig => (
          <Ionicons
            name="md-browsers"
            size={23}
            color={drawerConfig.tintColor}
          />
        )
      }
    },
    Filters: {
      screen: FiltersNavigator,
      navigationOptions: { drawerLabel: "Filter" }
    },
    Bookings: {
      screen: BookingsNavigator,
      navigationOptions: { drawerLabel: "Bookings" }
    },
    Admins: {
      screen: AdminNavigator,
      navigationOptions: { drawerLabel: "Service Admin" }
    }
  },
  {
    contentOptions: {
      activeTintColor: Colors.accentColor,
      labelStyle: {
        fontFamily: "open-sans-bold"
      }
    }
    // contentComponent: props => {
    //   const dispatch = useDispatch();
    //   return (
    //     <View style={{ flex: 1, paddingTop: 20 }}>
    //       <SafeAreaView forceInset={{ top: "always", horizontal: "never" }}>
    //         <DrawerItems {...props} />
    //         <Button
    //           title="Logout"
    //           color={Colors.primary}
    //           onPress={() => {
    //             dispatch(authActions.logout());
    //             // props.navigation.navigate('Auth');
    //           }}
    //         />
    //       </SafeAreaView>
    //     </View>
    //   );
    // }
  }
);

const AuthNavigator = createStackNavigator(
  {
    Auth: AuthScreen
  },
  {
    defaultNavigationOptions: defaultStackNavOptions
  }
);

//Todo: add a startup screen for auto login
const MainNavigator = createSwitchNavigator({
  Startup: StartupScreen,
  Auth: AuthNavigator,
  GroomUp: GroomUpNavigator
});

export default createAppContainer(MainNavigator);
