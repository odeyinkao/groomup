import React, { useState, useEffect, useCallback, useReducer } from "react";
import {
  View,
  ScrollView,
  StyleSheet,
  Platform,
  Alert,
  KeyboardAvoidingView,
  ActivityIndicator
} from "react-native";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import { useSelector, useDispatch } from "react-redux";

import HeaderButton from "../../components/UI/HeaderButton";
import * as servicesActions from "../../store/actions/services";
import Input from "../../components/UI/Input";
import SwitchInput from "../../components/UI/SwitchInput";
import Colors from "../../constants/Colors";
import LocationInput from "../../components/UI/LocationInput";

const FORM_INPUT_UPDATE = "FORM_INPUT_UPDATE";
const FORM_INPUT_UPDATE_LOCATION = "FORM_INPUT_UPDATE_LOCATION";

const formReducer = (state, action) => {
  if (action.type === FORM_INPUT_UPDATE) {
    const updatedValues = {
      ...state.inputValues,
      [action.input]: action.value
    };
    const updatedValidities = {
      ...state.inputValidities,
      [action.input]: action.isValid
    };
    let updatedFormIsValid = true;
    for (const key in updatedValidities) {
      updatedFormIsValid = updatedFormIsValid && updatedValidities[key];
    }
    console.log(updatedValidities);
    return {
      formIsValid: updatedFormIsValid,
      inputValidities: updatedValidities,
      inputValues: updatedValues
    };
  }
  if (action.type === FORM_INPUT_UPDATE_LOCATION) {
    // console.log(action);
    const updatedValues = {
      ...state.inputValues,
      altitude: action.value.altitude,
      latitude: action.value.latitude,
      longitude: action.value.longitude
    };

    const updatedValidities = {
      ...state.inputValidities,
      latitude: action.value.latitude ? true : false,
      longitude: action.value.longitude ? true : false
    };

    let updatedFormIsValid = true;
    for (const key in updatedValidities) {
      updatedFormIsValid = updatedFormIsValid && updatedValidities[key];
    }

    return {
      formIsValid: updatedFormIsValid,
      inputValidities: updatedValidities,
      inputValues: updatedValues
    };
  }
  return state;
};

const EditServiceScreen = props => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState();

  const serviceId = props.navigation.getParam("serviceId");
  const userService = useSelector(state => {
    // console.log(state.services.userService);
    return state.services.userService;
  });

  const dispatch = useDispatch();

  const [formState, dispatchFormState] = useReducer(formReducer, {
    inputValues: {
      title: userService ? userService.title : "",
      address: userService ? userService.address : "",
      imageUrl: userService ? userService.imageUrl : "10jpg",
      phoneNo: userService ? userService.phoneNo : "",
      allowChildren: userService ? userService.allowChildren : false,
      allowPet: userService ? userService.allowPet : false,
      color: userService ? userService.color : "blue",
      hasAirConditioner: userService ? userService.hasAirConditioner : false,
      latitude: userService ? userService.latitude : 0,
      longitude: userService ? userService.longitude : 0,
      altitude: userService ? userService.altitude : 0
    },
    inputValidities: {
      title: userService ? true : false,
      address: userService ? true : false,
      imageUrl: true,
      phoneNo: userService ? true : false,
      allowChildren: userService ? true : false,
      allowPet: userService ? true : false,
      color: true,
      hasAirConditioner: userService ? true : false,
      latitude: userService ? true : false,
      longitude: userService ? true : false
    },
    formIsValid: userService ? true : false
  });

  useEffect(() => {
    if (error) {
      Alert.alert("An error occurred!", error, [{ text: "Okay" }]);
    }
  }, [error]);

  const submitHandler = useCallback(async () => {
    console.log(formState.inputValues);
    console.log(formState.formIsValid);
    if (!formState.formIsValid) {
      Alert.alert("Wrong input!", "Please check the errors in the form.", [
        { text: "Okay" }
      ]);
      return;
    }
    setError(null);
    setIsLoading(true);
    try {
      if (userService) {
        await dispatch(
          servicesActions.updateService({
            id: serviceId,
            title: formState.inputValues.title,
            address: formState.inputValues.address,
            imageUrl: formState.inputValues.imageUrl,
            phoneNo: formState.inputValues.phoneNo,
            allowChildren: formState.inputValues.allowChildren,
            allowPet: formState.inputValues.allowPet,
            color: formState.inputValues.color,
            hasAirConditioner: formState.inputValues.hasAirConditioner,
            latitude: formState.inputValues.latitude,
            longitude: formState.inputValues.longitude,
            altitude: formState.inputValues.altitude
          })
        );
      } else {
        console.log("dshdsgh");
        await dispatch(
          servicesActions.createService({
            title: formState.inputValues.title,
            address: formState.inputValues.address,
            imageUrl: formState.inputValues.imageUrl,
            phoneNo: formState.inputValues.phoneNo,
            allowChildren: formState.inputValues.allowChildren,
            allowPet: formState.inputValues.allowPet,
            color: formState.inputValues.color,
            hasAirConditioner: formState.inputValues.hasAirConditioner,
            latitude: formState.inputValues.latitude,
            longitude: formState.inputValues.longitude,
            altitude: formState.inputValues.altitude
          })
        );
      }
      props.navigation.goBack();
    } catch (err) {
      setError(err.message);
    }

    setIsLoading(false);
  }, [dispatch, serviceId, formState]);

  useEffect(() => {
    props.navigation.setParams({ submit: submitHandler });
  }, [submitHandler]);

  const inputChangeHandler = useCallback(
    (inputIdentifier, inputValue, inputValidity) => {
      // console.log(inputValue);
      if (inputIdentifier === "location") {
        dispatchFormState({
          type: FORM_INPUT_UPDATE_LOCATION,
          value: inputValue,
          isValid: inputValidity
        });
        return;
      }

      dispatchFormState({
        type: FORM_INPUT_UPDATE,
        value: inputValue,
        isValid: inputValidity,
        input: inputIdentifier
      });
    },
    [dispatchFormState]
  );

  if (isLoading) {
    return (
      <View style={styles.centered}>
        <ActivityIndicator size="large" color={Colors.primary} />
      </View>
    );
  }

  return (
    <KeyboardAvoidingView
      style={{ flex: 1 }}
      behavior="padding"
      keyboardVerticalOffset={100}
    >
      <ScrollView>
        <View style={styles.form}>
          <Input
            id="title"
            label="Title"
            errorText="Title is required"
            keyboardType="default"
            autoCapitalize="sentences"
            autoCorrect
            returnKeyType="next"
            onInputChange={inputChangeHandler}
            initialValue={userService ? userService.title : ""}
            initiallyValid={!!userService}
            required
          />
          <Input
            id="address"
            label="Address"
            errorText="Address is required"
            keyboardType="default"
            autoCapitalize="sentences"
            autoCorrect
            returnKeyType="next"
            onInputChange={inputChangeHandler}
            initialValue={userService ? userService.address : ""}
            initiallyValid={!!userService}
            required
          />
          <Input
            id="imageUrl"
            label="Image Url"
            errorText="Please enter a valid image url!"
            keyboardType="default"
            returnKeyType="next"
            onInputChange={inputChangeHandler}
            initialValue={userService ? userService.imageUrl : "10jpg"}
            initiallyValid={!!userService}
            required
          />
          <Input
            id="phoneNo"
            label="Phone No "
            errorText="Please input your contact!"
            keyboardType="default"
            returnKeyType="next"
            onInputChange={inputChangeHandler}
            initialValue={userService ? userService.phoneNo : ""}
            initiallyValid={!!userService}
            required
          />
          <SwitchInput
            id="allowPet"
            label="Allow Pet ?"
            keyboardType="default"
            returnKeyType="next"
            onInputChange={inputChangeHandler}
            initialValue={userService ? userService.allowPet : false}
            initiallyValid={!!userService}
            required
          />
          <SwitchInput
            id="allowChildren"
            label="Allow Children?"
            keyboardType="default"
            returnKeyType="next"
            onInputChange={inputChangeHandler}
            initialValue={userService ? userService.allowChildren : false}
            initiallyValid={!!userService}
            required
          />
          <SwitchInput
            id="hasAirConditioner"
            label="Has HairCondition?"
            keyboardType="default"
            returnKeyType="next"
            onInputChange={inputChangeHandler}
            initialValue={userService ? userService.hasAirConditioner : false}
            initiallyValid={!!userService}
            required
          />
          <LocationInput
            id="location"
            label="Store Location?"
            onLocationSet={inputChangeHandler}
          />
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

EditServiceScreen.navigationOptions = navData => {
  const submitFn = navData.navigation.getParam("submit");
  return {
    headerTitle: navData.navigation.getParam("serviceId")
      ? "Edit Service"
      : "Add Service",
    headerRight: (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Save"
          iconName={
            Platform.OS === "android" ? "md-checkmark" : "ios-checkmark"
          }
          onPress={submitFn}
        />
      </HeaderButtons>
    )
  };
};

const styles = StyleSheet.create({
  form: {
    margin: 20
  },
  centered: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default EditServiceScreen;
