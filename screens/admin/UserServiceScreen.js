import React, { useEffect, useCallback, useState } from "react";
import {
  ScrollView,
  View,
  Image,
  Text,
  StyleSheet,
  Button,
  Platform,
  ActivityIndicator,
  TouchableOpacity
} from "react-native";
import { HeaderButtons, Item } from "react-navigation-header-buttons";

import { useSelector, useDispatch } from "react-redux";
import HeaderButton from "../../components/UI/HeaderButton";
import DefaultText from "../../components/UI/DefaultText";

import Colors from "../../constants/Colors";

import Images from "../../constants/LocalImageCollection";
import * as schedulesActions from "../../store/actions/services";
import ScheduleItem from "../../components/service/ScheduleItem";

// const ScheduleItem = props => {
//   return (
//     <View style={styles.ScheduleItem}>
//       <DefaultText>{props.item.date}</DefaultText>
//       <DefaultText>
//         {props.item.start} - {props.item.end}
//       </DefaultText>
//       <Button title="Book" />
//     </View>
//   );
// };

const UserServiceScreen = props => {
  const [isLoading, setIsLoading] = useState(false);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [error, setError] = useState();
  const dispatch = useDispatch();
  // const availableSchedules = useSelector(state => state.schedules.schedules);

  const myService = useSelector(state => state.services.userService);

  const loadMyServices = useCallback(async () => {
    setError(null);
    setIsRefreshing(true);

    try {
      await dispatch(schedulesActions.fetchMyService());
    } catch (err) {
      setError(err.message);
    }
    setIsRefreshing(false);
  }, [dispatch, setIsLoading, setError]);

  useEffect(() => {
    const willFocusSub = props.navigation.addListener(
      "willFocus",
      loadMyServices
    );

    return () => {
      willFocusSub.remove();
    };
  }, [loadMyServices]);

  useEffect(() => {
    props.navigation.setParams({ myService: myService });
  }, [myService]);

  useEffect(() => {
    setIsLoading(true);
    loadMyServices().then(() => {
      setIsLoading(false);
      // console.log(myService);
    });
  }, [dispatch, loadMyServices]);

  if (error) {
    console.log(error);
    return (
      <View style={styles.centered}>
        <DefaultText>An error occurred!</DefaultText>
        <Button
          title="Try again"
          onPress={loadMyServices}
          color={Colors.primary}
        />
      </View>
    );
  }

  if (isLoading) {
    return (
      <View style={styles.centered}>
        <ActivityIndicator size="large" color={Colors.primary} />
      </View>
    );
  }

  if (!isLoading && !myService) {
    return (
      <View style={styles.centered}>
        <DefaultText>
          There is no Service Details attached to this account?
        </DefaultText>
        <Button
          title="Create Service Details"
          onPress={() =>
            props.navigation.navigate({
              routeName: "EditService"
            })
          }
          color={Colors.primary}
        />
      </View>
    );
  }

  scheduleUpdate = id => {
    // navigation.
    // console.log(id);
  };

  return myService ? (
    <ScrollView>
      <TouchableOpacity
        onPress={() => {
          props.navigation.navigate({
            routeName: "EditService",
            params: myService ? { serviceId: myService.id } : null
          });
        }}
      >
        <Image source={Images[myService.imageUrl]} style={styles.image} />
        <View style={styles.details}>
          <DefaultText>{myService.address.toUpperCase()}</DefaultText>
          <DefaultText>{myService.phoneNo.toUpperCase()}</DefaultText>
        </View>
      </TouchableOpacity>
      <Text style={styles.title}>Schedules</Text>
      {myService.schedules.map(schedule => (
        <ScheduleItem
          key={schedule.id}
          item={schedule}
          serviceOwner={myService.id}
          actionLabel="Change"
          actionVisibleStatus="OPEN"
          actionFunction={() => {
            props.navigation.navigate({
              routeName: "EditSchedule",
              params: { scheduleId: schedule.id }
            });
          }}
        ></ScheduleItem>
      ))}
    </ScrollView>
  ) : null;
};

UserServiceScreen.navigationOptions = navData => {
  // const { state, setParams, navigate } = navData;
  let myService = navData.navigation.getParam("myService");
  // console.log(myService);
  // console.log(navData);
  return {
    headerTitle: "My Service Update",
    headerLeft: (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Menu"
          iconName="ios-menu"
          onPress={() => {
            navData.navigation.toggleDrawer();
          }}
        />
      </HeaderButtons>
    ),
    headerRight: myService ? (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Update"
          iconName={Platform.OS === "android" ? "playlist-edit" : "ios-attach"}
          onPress={() => {
            navData.navigation.navigate("EditSchedule");
          }}
        />
      </HeaderButtons>
    ) : null
  };
};

//props.navigation.popToTop();
//props.navigation.replace('Categories');
const styles = StyleSheet.create({
  image: {
    width: "100%",
    height: 200
  },
  details: {
    flexDirection: "row",
    padding: 15,
    justifyContent: "space-around"
  },
  title: {
    fontFamily: "open-sans-bold",
    fontSize: 22,
    textAlign: "center"
  },
  centered: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default UserServiceScreen;
