import React, { useState, useEffect, useCallback, useReducer } from "react";
import {
  View,
  ScrollView,
  StyleSheet,
  Platform,
  Alert,
  KeyboardAvoidingView,
  ActivityIndicator
} from "react-native";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import { useSelector, useDispatch } from "react-redux";

import HeaderButton from "../../components/UI/HeaderButton";
import * as schedulesActions from "../../store/actions/schedules";
import Input from "../../components/UI/Input";
import Picker from "../../components/UI/PickerInput";
import DateTimeInput from "../../components/UI/DateTimeInput";
import moment from "moment";
import Colors from "../../constants/Colors";

const FORM_INPUT_UPDATE = "FORM_INPUT_UPDATE";

const formReducer = (state, action) => {
  if (action.type === FORM_INPUT_UPDATE) {
    const updatedValues = {
      ...state.inputValues,
      [action.input]: action.value
    };

    const updatedValidities = {
      ...state.inputValidities,
      [action.input]: action.isValid
    };
    let updatedFormIsValid = true;
    for (const key in updatedValidities) {
      updatedFormIsValid = updatedFormIsValid && updatedValidities[key];
    }

    return {
      formIsValid: updatedFormIsValid,
      inputValidities: updatedValidities,
      inputValues: updatedValues
    };
  }
  return state;
};

const EditScheduleScreen = props => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState();

  const scheduleId = props.navigation.getParam("scheduleId");
  const editedSchedule = useSelector(state =>
    state.services.userService.schedules.find(prod => prod.id === scheduleId)
  );

  const categories = useSelector(state => state.categories.categories);
  // const editedSchedule = useSelector(state => state.schedules.userSchedule);

  const dispatch = useDispatch();

  const [formState, dispatchFormState] = useReducer(formReducer, {
    inputValues: {
      start: editedSchedule ? editedSchedule.start : "",
      end: editedSchedule ? editedSchedule.end : "",
      status: editedSchedule ? editedSchedule.status : "",
      categoryId: editedSchedule ? editedSchedule.categoryId : ""
    },
    inputValidities: {
      start: editedSchedule ? true : false,
      end: editedSchedule ? true : false,
      status: editedSchedule ? true : false,
      categoryId: editedSchedule ? true : false
    },
    formIsValid: editedSchedule ? true : false
  });

  useEffect(() => {
    if (error) {
      Alert.alert("An error occurred!", error, [{ text: "Okay" }]);
    }
  }, [error]);

  const submitHandler = useCallback(async () => {
    if (!formState.formIsValid) {
      Alert.alert("Wrong input!", "Please check the errors in the form.", [
        { text: "Okay" }
      ]);
      return;
    }
    setError(null);
    setIsLoading(true);
    try {
      if (editedSchedule) {
        await dispatch(
          schedulesActions.updateSchedule(
            scheduleId,
            formState.inputValues.start,
            formState.inputValues.end,
            formState.inputValues.status,
            formState.inputValues.categoryId
          )
        );
      } else {
        // console.log(formState);
        await dispatch(
          schedulesActions.createSchedule(
            formState.inputValues.start,
            formState.inputValues.end,
            formState.inputValues.status,
            formState.inputValues.categoryId
          )
        );
      }
      props.navigation.goBack();
    } catch (err) {
      setError(err.message);
    }

    setIsLoading(false);
  }, [dispatch, scheduleId, formState]);

  useEffect(() => {
    props.navigation.setParams({ submit: submitHandler });
  }, [submitHandler]);

  const inputChangeHandler = useCallback(
    (inputIdentifier, inputValue, inputValidity) => {
      dispatchFormState({
        type: FORM_INPUT_UPDATE,
        value: inputValue,
        isValid: inputValidity,
        input: inputIdentifier
      });
    },
    [dispatchFormState]
  );

  if (isLoading) {
    return (
      <View style={styles.centered}>
        <ActivityIndicator size="large" color={Colors.primary} />
      </View>
    );
  }

  let categoryItems = categories.map(item => {
    return { value: item.id, label: item.title };
  });

  let status = [
    { value: "OPEN", label: "OPEN" },
    { value: "CLOSED", label: "CLOSED" }
  ];

  return (
    <KeyboardAvoidingView
      style={{ flex: 1 }}
      behavior="padding"
      keyboardVerticalOffset={100}
    >
      <ScrollView>
        <View style={styles.form}>
          <DateTimeInput
            id="start"
            label="Start Time"
            errorText="Select a start Date and Time"
            keyboardType="default"
            returnKeyType="next"
            onInputChange={inputChangeHandler}
            initialValue={
              editedSchedule
                ? moment(editedSchedule.start).toDate()
                : new Date()
            }
            initiallyValid={!!editedSchedule}
            required
          />

          <DateTimeInput
            id="end"
            label="End Time"
            errorText="Select a end Date and Time"
            keyboardType="default"
            returnKeyType="next"
            onInputChange={inputChangeHandler}
            initialValue={
              editedSchedule ? moment(editedSchedule.end).toDate() : new Date()
            }
            initiallyValid={!!editedSchedule}
            required
          />
          <Picker
            id="status"
            label="Status"
            returnKeyType="next"
            onInputChange={inputChangeHandler}
            pickerItems={status}
            initialValue={editedSchedule ? editedSchedule.status : "OPEN"}
          />
          <Picker
            id="categoryId"
            label="Category"
            returnKeyType="next"
            keyboardType="default"
            onInputChange={inputChangeHandler}
            pickerItems={categoryItems}
            initialValue={editedSchedule ? editedSchedule.categoryId : 1}
          />
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

EditScheduleScreen.navigationOptions = navData => {
  const submitFn = navData.navigation.getParam("submit");
  return {
    headerTitle: navData.navigation.getParam("scheduleId")
      ? "Edit Schedule"
      : "Add Schedule",
    headerRight: (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Save"
          iconName={
            Platform.OS === "android" ? "md-checkmark" : "ios-checkmark"
          }
          onPress={submitFn}
        />
      </HeaderButtons>
    )
  };
};

const styles = StyleSheet.create({
  form: {
    margin: 20
  },
  centered: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default EditScheduleScreen;
