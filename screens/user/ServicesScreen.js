import React, { useState, useEffect, useCallback } from "react";
import { View, StyleSheet, ActivityIndicator, Button } from "react-native";
import { useSelector, useDispatch } from "react-redux";

import Colors from "../../constants/Colors";

import ServiceList from "../../components/service/ServiceList";
import DefaultText from "../../components/UI/DefaultText";
import * as servicesActions from "../../store/actions/services";

const ServicesScreen = props => {
  const categoryId = props.navigation.getParam("categoryId");

  const [isLoading, setIsLoading] = useState(false);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [error, setError] = useState();
  const dispatch = useDispatch();
  const availableServices = useSelector(
    state => state.services.availableServices
  );

  const loadServices = useCallback(async () => {
    setError(null);
    setIsRefreshing(true);
    try {
      await dispatch(servicesActions.fetchServices({ categoryId }));
    } catch (err) {
      setError(err.message);
    }
    setIsRefreshing(false);
  }, [dispatch, setIsLoading, setError]);

  useEffect(() => {
    const willFocusSub = props.navigation.addListener(
      "willFocus",
      loadServices
    );

    return () => {
      willFocusSub.remove();
    };
  }, [loadServices]);

  useEffect(() => {
    setIsLoading(true);
    loadServices().then(() => {
      setIsLoading(false);
    });
  }, [dispatch, loadServices]);

  if (error) {
    return (
      <View style={styles.centered}>
        <DefaultText>An error occurred!</DefaultText>
        <Button
          title="Try again"
          onPress={loadServices}
          color={Colors.primary}
        />
      </View>
    );
  }

  if (isLoading) {
    return (
      <View style={styles.centered}>
        <ActivityIndicator size="large" color={Colors.primary} />
      </View>
    );
  }

  if (!isLoading && availableServices.length === 0) {
    return (
      <View style={styles.centered}>
        <DefaultText>No service provider found?</DefaultText>
      </View>
    );
  }

  return (
    <ServiceList
      style={styles.centered}
      listData={availableServices}
      navigation={props.navigation}
    />
  );
};

ServicesScreen.navigationOptions = navigationData => {
  const title = navigationData.navigation.getParam("categoryTitle");

  return {
    headerTitle: title
  };
};

const styles = StyleSheet.create({
  centered: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default ServicesScreen;
