import React from "react";
import { View, Text, StyleSheet } from "react-native";
import ServiceList from "../../components/service/ServiceList";

import { useSelector } from "react-redux";
import { HeaderButtons, Item } from "react-navigation-header-buttons";

import HeaderButton from "../../components/UI/HeaderButton";
import DefaultText from "../../components/UI/DefaultText";

const FavoritesScreen = props => {
  const favoriteServices = useSelector(state => {
    console.log(state.services.favoriteServices);
    return state.services.favoriteServices;
  });

  if (!favoriteServices || favoriteServices.length === 0) {
    return (
      <View style={styles.content}>
        <DefaultText>No Favorite Service found. Start adding some</DefaultText>
      </View>
    );
  }

  return (
    <ServiceList listData={favoriteServices} navigation={props.navigation} />
  );
};

const styles = StyleSheet.create({
  content: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

FavoritesScreen.navigationOptions = navData => {
  return {
    headerTitle: "Your Favorites",
    headerLeft: (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Menu"
          iconName="ios-menu"
          onPress={() => {
            navData.navigation.toggleDrawer();
          }}
        />
      </HeaderButtons>
    )
  };
};

export default FavoritesScreen;
