import React, { useEffect, useCallback, useState } from "react";
import {
  ScrollView,
  View,
  Image,
  Text,
  StyleSheet,
  Button,
  ActivityIndicator
} from "react-native";
import { HeaderButtons, Item } from "react-navigation-header-buttons";

import { useSelector, useDispatch } from "react-redux";
import HeaderButton from "../../components/UI/HeaderButton";
import DefaultText from "../../components/UI/DefaultText";

import Colors from "../../constants/Colors";
import { toggleFavorite } from "../../store/actions/services";

import Images from "../../constants/LocalImageCollection";
import * as schedulesActions from "../../store/actions/schedules";
import ScheduleItem from "../../components/service/ScheduleItem";

// const ScheduleItem = props => {
//   return (
//     <View style={styles.ScheduleItem}>
//       <DefaultText>{props.item.date}</DefaultText>
//       <DefaultText>
//         {props.item.start} - {props.item.end}
//       </DefaultText>
//       <Button title="Book" />
//     </View>
//   );
// };

const ServiceDetailScreen = props => {
  const [isLoading, setIsLoading] = useState(false);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [error, setError] = useState();
  const dispatch = useDispatch();

  const availableSchedules = useSelector(state => state.schedules.schedules);

  let schedulesView = "";
  const serviceId = props.navigation.getParam("serviceId");
  let service = null;

  const bookSchedule = (id, serviceOwner) => {
    // console.log("Here now");
    // console.log(data);
    console.log(id);
    console.log(serviceOwner);
    dispatch(schedulesActions.bookSchedule(id, serviceOwner));
  };

  useEffect(() => {
    console.log(availableSchedules); //console educations after every change
  });

  const loadSchedules = useCallback(async () => {
    setError(null);
    setIsRefreshing(true);

    try {
      await dispatch(schedulesActions.fetchSchedules({ serviceId }));
    } catch (err) {
      setError(err.message);
    }
    setIsRefreshing(false);
  }, [dispatch, setIsLoading, setError]);

  useEffect(() => {
    const willFocusSub = props.navigation.addListener(
      "willFocus",
      loadSchedules
    );

    return () => {
      willFocusSub.remove();
    };
  }, [loadSchedules]);

  useEffect(() => {
    setIsLoading(true);
    loadSchedules().then(() => {
      setIsLoading(false);
    });
  }, [dispatch, loadSchedules]);

  if (error) {
    schedulesView = (
      <View style={styles.centered}>
        <DefaultText>An error occurred!</DefaultText>
        <Button
          title="Try again"
          onPress={loadSchedules}
          color={Colors.primary}
        />
      </View>
    );
  }

  if (isLoading) {
    schedulesView = (
      <View style={styles.centered}>
        <ActivityIndicator size="large" color={Colors.primary} />
      </View>
    );
  }

  if (!isLoading && availableSchedules.length === 0) {
    schedulesView = (
      <View style={styles.centered}>
        <DefaultText>
          No schedule find yet under this provider and category
        </DefaultText>
      </View>
    );
  }

  //   if (!isLoading && availableSchedules.length > 0) {

  // ;
  //   }

  const selectedServices = useSelector(
    state => state.services.availableServices
  );

  const selectedService = selectedServices.find(
    service => service.id === serviceId
  );

  // const availableSchedules = useSelector(
  //   state => state.services.availableSchedules
  // );

  // const currentServiceIsFavorite = useSelector(state =>
  //   state.services.favoriteServices.some(service => service.id === serviceId)
  // );

  const toggleFavoriteHandler = useCallback(() => {
    dispatch(toggleFavorite(serviceId));
  }, [dispatch, serviceId]);

  useEffect(() => {
    props.navigation.setParams({ toggleFavorite: toggleFavoriteHandler });
  }, [toggleFavoriteHandler]);

  // useEffect(() => {
  //   props.navigation.setParams({ isFavorite: currentServiceIsFavorite }, []);
  // }, [currentServiceIsFavorite]);

  return selectedService ? (
    <ScrollView>
      <Image source={Images[selectedService.imageUrl]} style={styles.image} />
      <View style={styles.details}>
        <DefaultText>{selectedService.address.toUpperCase()}</DefaultText>
        <DefaultText>{selectedService.phoneNo.toUpperCase()}</DefaultText>
      </View>
      <Text style={styles.title}>Schedules</Text>
      {schedulesView ||
        availableSchedules.map(schedule => (
          <ScheduleItem
            key={schedule.id}
            item={schedule}
            serviceOwner={selectedService.id}
            actionLabel="Book"
            actionVisibleStatus="OPEN"
            actionFunction={bookSchedule}
          ></ScheduleItem>
        ))}
    </ScrollView>
  ) : null;
};
ServiceDetailScreen.navigationOptions = navigationData => {
  const serviceTitle = navigationData.navigation.getParam("serviceTitle");
  const toggleFavorite = navigationData.navigation.getParam("toggleFavorite");
  const isFavorite = navigationData.navigation.getParam("isFavorite");

  return {
    headerTitle: serviceTitle,
    headerRight: (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Favorite"
          iconName={isFavorite ? "ios-star" : "ios-star-outline"}
          onPress={toggleFavorite}
        />
      </HeaderButtons>
    )
  };
};
//props.navigation.popToTop();
//props.navigation.replace('Categories');
const styles = StyleSheet.create({
  image: {
    width: "100%",
    height: 200
  },
  details: {
    flexDirection: "row",
    padding: 15,
    justifyContent: "space-around"
  },
  title: {
    fontFamily: "open-sans-bold",
    fontSize: 22,
    textAlign: "center"
  },
  centered: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default ServiceDetailScreen;
