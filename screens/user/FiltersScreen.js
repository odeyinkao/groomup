import React, { useState, useEffect, useCallback } from "react";
import { View, Text, Switch, StyleSheet, Platform } from "react-native";
import { useDispatch } from "react-redux";

import { HeaderButtons, Item } from "react-navigation-header-buttons";
import HeaderButton from "../../components/UI/HeaderButton";

import { setFilters } from "../../store/actions/services";
import FilterSwitch from "../../components/UI/SwitchInput";

const FiltersScreen = props => {
  const [hasAirConditioner, sethasAirConditioner] = useState(false);
  const [allowPet, setAllowPet] = useState(false);
  const [allowChildren, setAllowChildren] = useState(false);

  const dispatch = useDispatch();

  const saveFilters = useCallback(() => {
    const appliedFilters = {
      hasAirConditioner: hasAirConditioner,
      allowPet: allowPet,
      allowChildren: allowChildren
    };

    dispatch(setFilters(appliedFilters));
  }, [hasAirConditioner, allowPet, allowChildren]);

  useEffect(() => {
    props.navigation.setParams({ save: saveFilters });
  }, [saveFilters]);

  return (
    <View style={styles.screen}>
      <Text style={styles.title}>Available Filters / Restrictions</Text>
      <FilterSwitch
        id="has_airConditioner"
        label="Has-AirConditioner"
        state={hasAirConditioner}
        onInputChange={newValue => sethasAirConditioner(newValue)}
      />
      <FilterSwitch
        id="allow_pet"
        label="Allow-Pet"
        state={allowPet}
        onInputChange={newValue => setAllowPet(newValue)}
      />
      <FilterSwitch
        id="allow_pet"
        label="Allow-Children"
        state={allowChildren}
        onInputChange={newValue => setAllowChildren(newValue)}
      />
    </View>
  );
};

FiltersScreen.navigationOptions = navData => {
  return {
    headerTitle: "Filtered Services",
    headerLeft: (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Menu"
          iconName="ios-menu"
          onPress={() => {
            navData.navigation.toggleDrawer();
          }}
        />
      </HeaderButtons>
    ),
    headerRight: (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Save"
          iconName="ios-save"
          onPress={navData.navigation.getParam("save")}
        />
      </HeaderButtons>
    )
  };
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    alignItems: "center"
  },
  title: {
    fontSize: 22,
    fontFamily: "open-sans-bold",
    margin: 20,
    textAlign: "center"
  }
});

export default FiltersScreen;
