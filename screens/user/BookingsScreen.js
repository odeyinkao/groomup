import React, { useState, useEffect, useCallback } from "react";
import {
  View,
  FlatList,
  Text,
  Platform,
  ActivityIndicator,
  StyleSheet,
  Button
} from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { HeaderButtons, Item } from "react-navigation-header-buttons";

import DefaultText from "../../components/UI/DefaultText";
import * as bookingsActions from "../../store/actions/bookings";
import BookingItem from "../../components/service/BookingItem";

import HeaderButton from "../../components/UI/HeaderButton";
import Colors from "../../constants/Colors";

const BookingsScreen = props => {
  const [isLoading, setIsLoading] = useState(false);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [error, setError] = useState();

  let bookings = null;
  if (true) {
    bookings = useSelector(state => state.bookings.client_bookings);
  } else {
    bookings = useSelector(state => state.bookings.bookings);
  }
  const dispatch = useDispatch();

  const loadServices = useCallback(async () => {
    setError(null);
    setIsRefreshing(true);
    try {
      if (true) {
        await dispatch(bookingsActions.fetchBookingForClients());
      } else {
        await dispatch(bookingsActions.fetchBookings());
      }
    } catch (err) {
      setError(err.message);
    }
    setIsRefreshing(false);
  }, [dispatch, setIsLoading, setError]);

  useEffect(() => {
    const willFocusSub = props.navigation.addListener(
      "willFocus",
      loadServices
    );

    return () => {
      willFocusSub.remove();
    };
  }, [loadServices]);

  useEffect(() => {
    setIsLoading(true);
    loadServices().then(() => {
      setIsLoading(false);
    });
  }, [dispatch, loadServices]);

  if (error) {
    return (
      <View style={styles.centered}>
        <DefaultText>An error occurred!</DefaultText>
        <Button
          title="Try again"
          onPress={loadServices}
          color={Colors.primary}
        />
      </View>
    );
  }

  if (isLoading) {
    return (
      <View style={styles.centered}>
        <ActivityIndicator size="large" color={Colors.primary} />
      </View>
    );
  }

  if (bookings.length === 0) {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Text>No booking found, maybe start booking some services?</Text>
      </View>
    );
  }

  return (
    <FlatList
      data={bookings}
      keyExtractor={item => item.id}
      renderItem={bookingItem => (
        <BookingItem
          key={bookingItem.id}
          item={bookingItem}
          // actionLabel="Book"
          // actionVisibleStatus="OPEN"
          // actionFunction={bookSchedule}
        />
      )}
    />
  );
};

BookingsScreen.navigationOptions = navData => {
  return {
    headerTitle: "Your Bookings",
    headerLeft: (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Menu"
          iconName={Platform.OS === "android" ? "md-menu" : "ios-menu"}
          onPress={() => {
            navData.navigation.toggleDrawer();
          }}
        />
      </HeaderButtons>
    )
  };
};

const styles = StyleSheet.create({
  centered: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default BookingsScreen;
