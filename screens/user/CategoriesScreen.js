import React, { useState, useEffect, useCallback } from "react";
import { View, StyleSheet, ActivityIndicator, Button } from "react-native";
import { FlatList } from "react-native-gesture-handler";
import { HeaderButtons, Item } from "react-navigation-header-buttons";

import { useSelector, useDispatch } from "react-redux";
import HeaderButton from "../../components/UI/HeaderButton";
import CategoryGridTile from "../../components/service/CategoryGridTile";
import DefaultText from "../../components/UI/DefaultText";
import * as categoriesActions from "../../store/actions/categories";
import Colors from "../../constants/Colors";

const CategoriesScreen = props => {
  const [isLoading, setIsLoading] = useState(false);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [error, setError] = useState();
  const dispatch = useDispatch();
  const categories = useSelector(state => state.categories.categories);
  const user = useSelector(state => state.auth.user);

  const loadCategories = useCallback(async () => {
    setError(null);
    setIsRefreshing(true);

    try {
      console.log("Here nono ");
      await dispatch(categoriesActions.fetchCategories());
    } catch (err) {
      console.log(err);
      setError(err.message);
    }
    setIsRefreshing(false);
  }, [dispatch, setIsLoading, setError]);

  useEffect(() => {
    const willFocusSub = props.navigation.addListener(
      "willFocus",
      loadCategories
    );

    return () => {
      willFocusSub.remove();
    };
  }, [loadCategories]);

  useEffect(() => {
    console.log(user);
    setIsLoading(true);
    loadCategories().then(() => {
      setIsLoading(false);
    });
  }, [dispatch, user]);

  useEffect(() => {
    setIsLoading(true);
    loadCategories().then(() => {
      setIsLoading(false);
    });
  }, [dispatch, loadCategories]);

  if (error) {
    // console.log(error);
    return (
      <View style={styles.centered}>
        <DefaultText>An error occurred!</DefaultText>
        <Button
          title="Try again"
          onPress={loadCategories}
          color={Colors.primary}
        />
      </View>
    );
  }

  if (isLoading) {
    return (
      <View style={styles.centered}>
        <ActivityIndicator size="large" color={Colors.primary} />
      </View>
    );
  }

  if (!isLoading && categories.length === 0) {
    return (
      <View style={styles.centered}>
        <DefaultText>No products found., maybe check your filters?</DefaultText>
      </View>
    );
  }

  const renderGridItem = itemData => {
    return (
      <CategoryGridTile
        title={itemData.item.title}
        color={itemData.item.color}
        onSelect={() => {
          console.log("Here Now");
          console.log(itemData);
          props.navigation.navigate({
            routeName: "CategoryServices",
            params: {
              categoryId: itemData.item.id,
              categoryTitle: itemData.item.title
            }
          });
        }}
      />
    );
  };

  return (
    <FlatList
      keyExtractor={(item, index) => item.id}
      data={categories}
      renderItem={renderGridItem}
      numColumns={2}
    />
  );
};

CategoriesScreen.navigationOptions = navData => {
  return {
    headerTitle: "Service Categories",
    headerLeft: (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Menu"
          iconName="ios-menu"
          onPress={() => {
            navData.navigation.toggleDrawer();
          }}
        />
      </HeaderButtons>
    )
  };
};

const styles = StyleSheet.create({
  centered: { flex: 1, justifyContent: "center", alignItems: "center" },
  screen: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default CategoriesScreen;
