class Booking {
  constructor(data) {
    // this.id = data.id;
    // this.status = data.status;
    // this.start = data.period.start;
    // this.end = data.period.end;
    // this.serviceProviderId = data.serviceProviderId;
    // this.categoryId = data.categoryId;
    // this.categoryId = data.categoryId;

    this.id = data.id;
    this.status = data.status;
    this.start = data.period.start;
    this.end = data.period.end;
    this.serviceProviderId = data.serviceProviderId;
    this.categoryId = data.categoryId;

    this.amount = data.transaction && data.transaction.amount;
    this.date = data.transaction && data.transaction.date;
    this.comment = data.transaction && data.transaction.comment;
  }
}

export default Booking;
