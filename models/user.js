class User {
  constructor(data) {
    this.id = data ? data.id : null;
    this.name = data ? data.name : null;
    this.photoUrl = data ? data.photoUrl : null;
    this.token = data ? data.token : null;
    this.role = data ? data.role : null;
    this.sourceType = data ? data.sourceType : null;
    // this.favorite = map favorite providers here;
  }
}

export default User;
