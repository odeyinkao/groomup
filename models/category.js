class Category {
  constructor(data) {
    this.id = data.id;
    this.title = data.title;
    this.color = data.color;
    this.url = data.url;
  }
}

export default Category;
