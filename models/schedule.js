class Schedule {
  constructor(data) {
    this.id = data.id;
    this.status = data.status;
    this.start = data.period.start;
    this.end = data.period.end;
    this.serviceProviderId = data.serviceProviderId;
    this.categoryId = data.categoryId;
    // this.categoryId = data.categoryId;
  }
}

export default Schedule;
// {
//   "id": 2,
//   "ownerId": null,
//   "period": Object {
//     "end": "2019-08-23T13:30:00",
//     "start": "2019-08-23T13:00:00",
//   },
//   "serviceProviderId": 1,
//   "status": "OPEN",
// },
// "start": "12:00",
// },
