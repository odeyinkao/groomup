class Service {
  constructor(data) {
    this.id = data.id + "";
    this.linkedUserId = data.userId;
    this.title = data.title;
    this.address = data.address;
    this.color = data.color;
    this.phoneNo = data.phoneNo;
    this.imageUrl = data.imageUrl;
    this.hasAirConditioner = data.hasAirConditioner;
    this.allowPet = data.allowPet;
    this.allowChildren = data.allowChildren;
    this.latitude = data.latitude;
    this.longitude = data.longitude;
    this.altitude = data.altitude;
    this.accuracy = data.accuracy;

    this.schedules = [];
  }
}

export default Service;
