import React, { useState } from "react";
import { createStore, combineReducers, applyMiddleware } from "redux";
import * as Font from "expo-font";
import ReduxThunk from "redux-thunk";
import { AppLoading } from "expo";
import { useScreens } from "react-native-screens";
import { Provider } from "react-redux";

import NavigationContainer from "./navigation/NavigationContainer";
import servicesReducer from "./store/reducers/services";
import schedulesReducer from "./store/reducers/schedules";
import categoriesReducer from "./store/reducers/categories";
import authReducer from "./store/reducers/auth";
import bookingsReducer from "./store/reducers/bookings";

useScreens();

const rootReducer = combineReducers({
  services: servicesReducer,
  schedules: schedulesReducer,
  categories: categoriesReducer,
  bookings: bookingsReducer,
  auth: authReducer
});

const store = createStore(rootReducer, applyMiddleware(ReduxThunk));

const fetchFonts = () => {
  return Font.loadAsync({
    "open-sans": require("./assets/fonts/OpenSans-Regular.ttf"),
    "open-sans-bold": require("./assets/fonts/OpenSans-Bold.ttf")
  });
};

export default function App() {
  const [fontLoaded, setFontLoaded] = useState(false);

  if (!fontLoaded) {
    return (
      <AppLoading
        startAsync={fetchFonts}
        onFinish={() => setFontLoaded(true)}
      />
    );
  }

  return (
    <Provider store={store}>
      <NavigationContainer />
    </Provider>
  );
}
